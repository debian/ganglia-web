The Debian security team has decided that security support for ganglia-web
will be limited only to installations behind a trusted HTTP zone.  Thus,
any problems affecting this package in non-trusted zones will not receive
priority remediation through the Debian security team and will be handled
as normal bugs by the package maintainer.

This software was written under the expectation that it be used in a
local cluster environment, and thus the security consequences of
exposing various interfaces to the wider untrusted internet were not
fully considered, and are consequently not handled well. The security
team does not have the time or resources to compensate for these
upstream limitations.

The security team's decision is documented here:

http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=702775

As best practice, it is recommended that users concerned about security
consider the following:

- subscribe to the upstream mailing list for alerts about security issues

- use HTTP authentication and/or ACL schemes to protect the Ganglia pages

