<?php
if( file_exists( "/etc/ganglia-webfrontend/conf.php" ) ) {
  include_once "/etc/ganglia-webfrontend/conf.php";
}
$conf['jquery_js_path'] = "/javascript/jquery/jquery.min.js";
$conf['jquerymobile_js_path'] = "/javascript/jquery-mobile/jquery.mobile.min.js";
$conf['jqueryui_js_path'] = "/javascript/jquery-ui/jquery-ui.min.js";
$conf['jqueryui_smoothness_css_path'] = "/javascript/jquery-ui/themes/base/all.min.css";
$conf['jquery_scrollTo_js_path'] = "/javascript/jquery-scrollto/jquery.scrollTo.min.js";
$conf['jquery_cookie_js_path'] = "/javascript/jquery-cookie/jquery.cookie.min.js";
$conf['rickshaw_js_path'] = "/javascript/rickshaw/rickshaw.min.js";
$conf['cubism_js_path'] = "/ganglia/js/cubism.v1.min.js";
$conf['d3_js_path'] = "/javascript/d3/d3.min.js";
$conf['protovis_js_path'] = "/ganglia/js/protovis.min.js";
$conf['jstree_js_path'] = "/javascript/jstree/jstree.min.js";
$conf['jstree_css_path'] = "/javascript/jstree/themes/default/style.css";
$conf['chosen_js_path'] = "/javascript/chosen/chosen.jquery.min.js";
$conf['chosen_css_path'] = "/javascript/chosen/chosen.min.css";
$conf['jstz_js_path'] = "/javascript/jstimezonedetect/jstz.min.js";
$conf['moment_js_path'] = "/javascript/moment/moment.min.js";
$conf['moment-timezone_js_path'] = "/javascript/moment-timezone/moment-timezone-with-data.min.js";
$conf['select2_js_path'] = "/javascript/select2.js/select2.min.js";
$conf['select2_css_path'] = "/javascript/select2.js/select2.min.css";
$conf['jquery_visible_js_path'] = "js/jquery.visible.min.js";
$conf['fullcalendar_js_path'] = "js/fullcalendar.min.js";
$conf['fullcalendar_css_path'] = "js/fullcalendar.min.css";
$conf['jquery_flot_base_path'] = "/javascript/jquery-flot/jquery.flot";
?>
